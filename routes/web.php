<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('/auth/login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

// Customers
Route::get('/customers', 'CustomersController@index')->name('customers.index');
Route::get('/customers/edit/{id}', 'CustomersController@edit')->name('customers.edit');
Route::get('/customers/update/{id}', 'CustomersController@update')->name('customers.update');
Route::get('/customers/getDataCustomers', 'CustomersController@getDataCustomers')->name('customers.getDataCustomers');
Route::post('/customers/create', 'CustomersController@create')->name('customers.create');

// Categories
Route::get('/categories', 'CategoriesController@index')->name('categories.index');
Route::get('/categories/edit/{id}', 'CategoriesController@edit')->name('categories.edit');
Route::get('/categories/update/{id}', 'CategoriesController@update')->name('categories.update');
Route::get('/categories/getDataCategories', 'CategoriesController@getDataCategories')->name('categories.getDataCategories');
Route::post('/categories/store', 'CategoriesController@store')->name('categories.store');
Route::post('/categories/create', 'CategoriesController@create')->name('categories.create');
Route::delete('/destroy/{Id}', 'CategoriesController@destroy');

// Items
Route::get('/items', 'ItemsController@index')->name('items.index');
Route::get('/items/edit/{id}', 'ItemsController@edit')->name('items.edit');
Route::get('/items/show/{id}', 'ItemsController@show')->name('items.show');
Route::get('/items/getDataItems', 'ItemsController@getDataItems')->name('items.getDataItems');
Route::post('/items/store', 'ItemsController@store')->name('items.store');
Route::post('/items/create', 'ItemsController@create')->name('items.create');
Route::post('/items/update', 'ItemsController@update')->name('items.update');
Route::delete('/destroy/{Id}', 'ItemsController@destroy');

// Carts
Route::get('/cart/store', 'CartsController@store')->name('cart.store');
Route::get('/cart/update/{id}', 'CartsController@update')->name('cart.update');

// Carts Detail
Route::get('/carts-detail', 'CartsDetailController@index')->name('cartsDetail.index');
Route::get('/carts-detail/show/{id}', 'CartsDetailController@show')->name('cartsDetail.show');
Route::get('/carts-detail/download/{id_cart}', 'CartsDetailController@download')->name('cartsDetail.download');

// API Categories
Route::get('categories-api/store', 'CategoriesApiController@store');

// API Items
Route::get('items-api/index', 'ItemsApiController@index');
Route::get('items-api/show/{id}', 'ItemsApiController@show');
Route::get('items-api/update/{id}', 'ItemsApiController@update');
Route::get('items-api/destroy/{id}', 'ItemsApiController@destroy');
Route::get('items-api/store', 'ItemsApiController@store');

// API Customers
Route::get('customers-api/store', 'CustomersApiController@store');
Route::get('customers-api/show/{id}', 'CustomersApiController@show');
Route::get('customers-api/update/{id}', 'CustomersApiController@update');
Route::get('customers-api/destroy/{id}', 'CustomersApiController@destroy');

// API CART
Route::get('carts-api/store', 'CartsApiController@store');

// API CART DETAIL
Route::get('carts-detail-api/show/{id}', 'CartsDetailApiController@show');