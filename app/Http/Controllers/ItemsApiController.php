<?php

namespace App\Http\Controllers;

use App\Models\Items;
use Illuminate\Http\Request;

class ItemsApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return csrf_token(); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        
        $this->validate($request,[
            'id_categorie',
            'name',
            'price',
            'image',
        ]);

        $badChars       = array(".");
        $price          = str_ireplace($badChars, "", $request->price);
        $time           = time();

        if ($request->file('image') != NULL){
            $image              = $request->file('image');
            $new_name_image     = time().'.'.$image->extension();

            $path   = 'images/products/';

            $request->file('image')->move($path,$new_name_image);
        }
        else{
            $new_name_image = 'no_image.png';
        }

        $items = Items::create([
            'id_items'          => $request->id_categories.$time,
            'id_categorie'      => $request->id_categorie,
            'name'              => $request->name,
            'price'             => $price,
            'image'             => $new_name_image,
        ]);


        return response()->json($items, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $items = Items::where('id', $id)->first();

        return response()->json($items, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'id_categorie',
            'name',
            'price',
            'image',
        ]);
        
        $data           = Items::where('id', $request->id)->first();

        $badChars       = array(".");
        $price          = str_ireplace($badChars, "", $request->price);

        if ($request->file('image') != NULL){
            $image              = $request->file('image');
            $time               = time();
            $new_name_image     = time().'.'.$image->extension();

            $path   = 'images/products/';

            $request->file('image')->move($path,$new_name_image);
            $data->image    = $new_name_image;
        }

        $data->id_items         = $request->id_items;
        $data->id_categorie     = $request->id_categorie;
        $data->name             = $request->name;
        $data->price            = $price;

        $data->save();

        return response()->json($data, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data   = Items::where('id',$id)->delete();

        return response()->json($data, 204);
    }
}
