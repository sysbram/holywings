<?php

namespace App\Http\Controllers;

use App\Models\Carts;
use App\Models\CartsDetail;
use App\Models\Items;
use Illuminate\Http\Request;
use Auth;

class CartsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'id_items',
            'quantity',
        ]);

        if (Auth::id()){
            $id_customer    = Auth::id();
        }
        else{
            return redirect()->route('login');
        }

        $carts      = Carts::where('id_customer', $id_customer)->where('status', 1)->first();
        $items      = Items::where('id_items', $request->id_items)->first();
        if($carts){
            $total  = $request->quantity * $items->price;
            
            CartsDetail::create([
                'id_cart'       => $carts->id,
                'id_customer'   => $carts->id_customer,
                'id_item'       => $request->id_items,
                'quantity'      => $request->quantity,
                'total'         => $total,
            ]);
        }
        else{
            Carts::create([
                'id_customer'   => $id_customer,
            ]);

            $data_carts     = $carts   = Carts::where('id_customer', $id_customer)->where('status', 1)->first();
            
            
            $total  = $request->quantity * $items->price;

            CartsDetail::create([
                'id_cart'       => $data_carts->id,
                'id_customer'   => $data_carts->id_customer,
                'id_item'       => $request->id_items,
                'quantity'      => $request->quantity,
                'total'         => $total,
            ]);

        }

        return redirect()->route('cartsDetail.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'id_cart',
        ]);

        if (Auth::id()){
            $id_customer    = Auth::id();
        }
        else{
            return redirect()->route('login');
        }

        $carts   = Carts::where('id_customer', $id_customer)->where('id', $id)->first();

        $carts->status = 2;

        $carts->save();

        return redirect()->route('cartsDetail.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
