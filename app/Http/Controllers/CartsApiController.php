<?php

namespace App\Http\Controllers;

use App\Models\Carts;
use App\Models\CartsDetail;
use App\Models\Items;
use Illuminate\Http\Request;
use Auth;

class CartsApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'id_item',
            'id_customer',
            'id_cart',
            'quantity',
        ]);

        $carts      = Carts::where('id_customer', $request->id_customer)->where('status', 1)->first();
        $items      = Items::where('id_items', $request->id_item)->first();
        if($carts){
            $total  = $request->quantity * $items->price;
            
            $cart_detail = CartsDetail::create([
                'id_cart'       => $carts->id,
                'id_customer'   => $carts->id_customer,
                'id_item'       => $request->id_item,
                'quantity'      => $request->quantity,
                'total'         => $total,
            ]);
        }
        else{
            Carts::create([
                'id_customer'   => $request->id_customer,
            ]);

            $data_carts     = $carts   = Carts::where('id_customer', $request->id_customer)->where('status', 1)->first();
            
            
            $total  = $request->quantity * $items->price;

            $cart_detail = CartsDetail::create([
                'id_cart'       => $data_carts->id,
                'id_customer'   => $data_carts->id_customer,
                'id_item'       => $request->id_item,
                'quantity'      => $request->quantity,
                'total'         => $total,
            ]);

        }

        return response()->json($cart_detail, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $id_customer    = Auth::id();
        $customer       = Customers::where('id', $id_customer)->first();
        $cart_detail    = CartsDetail::where('id_cart', $id)->get();
        
        return view('carts-detail/invoice',[
            'cart_detail'       => $cart_detail,
            'customer'          => $customer,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
