<?php

namespace App\Http\Controllers;

use App\Models\Categories;
use Yajra\Datatables\Datatables;
use Illuminate\Http\Request;

class CategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('categories/index');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getDataCategories(){
        $categories  = Categories::all();

        return Datatables::of($categories)
        ->addColumn('action', function ($categories){
            return
                '<a href=" '.route('categories.edit', $categories->id). '" title="Edit" <i class="fa fa-edit  text-primary" aria-hidden="true"></i></a>'." ". 
                '<a href="javascript:void(0)" data-toggle="modal" data-target="#deleteModal" data-id="'.$categories->id.'" class="text-danger"><i class="fa fa-trash"></i></a>'
            ;
        })
        ->rawColumns(['action'])
        ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'id_categorie',
            'name',
        ]);

        Categories::create([
            'id_categorie'  => $request->id_categorie,
            'name'          => $request->name,
        ]);

        return redirect()->route('categories.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model  = Categories::where('id', $id)->first();

        return view('categories/edit',[
            'model' => $model,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'id_categorie',
            'name',
        ]);
        
        $data   = Categories::where('id', $id)->first();

        $data->id_categorie     = $request->id_categorie;
        $data->name             = $request->name;

        $data->save();

        return redirect()->route('categories.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {  
        $data   = Categories::where('id', $id)->delete();

        return response()->json($data);
    }
}
