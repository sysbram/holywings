<?php

namespace App\Http\Controllers;

use App\Models\Customers;
use Yajra\Datatables\Datatables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class CustomersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getDataCustomers(){
        $customers  = Customers::where('id', '!=', 1)->get();

        return Datatables::of($customers)
        ->addColumn('action', function ($customers){
            return
                '<a href=" '.route('customers.edit', $customers->id). '" title="Edit" <i class="fa fa-edit  text-primary" aria-hidden="true"></i></a>'." ". 
                '<a href="javascript:void(0)" data-toggle="modal" data-target="#deleteModal" data-id="'.$customers->id.'" class="text-danger"><i class="fa fa-trash"></i></a>'
            ;
        })
        ->rawColumns(['action'])
        ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $password   = Hash::make($request->password);

        $this->validate($request,[
            'name',
            'no_handphone',
            'password',
            'address',
        ]);

        Customers::create([
            'name'              => $request->name,
            'no_handphone'      => $request->no_handphone,
            'password'          => Hash::make($request->password),
            'address'           => $request->address,
        ]);

        return redirect()->route('customers.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model  = Customers::where('id', $id)->first();

        return view('customers/edit',[
            'model' => $model,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name',
            'no_handphone',
            'address',
        ]);

        $data = Customers::where('id', $id)->first();

        $data->name             = $request->name;
        $data->no_handphone     = $request->no_handphone;
        $data->address          = $request->address;

        $data->save();

        return redirect()->route('customers.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
