<?php

namespace App\Http\Controllers;

use App\Models\Carts;
use App\Models\CartsDetail;
use App\Models\Customers;
use Illuminate\Http\Request;
use Auth;
use PDF;

class CartsDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::id()){
            $id_customer        = Auth::id();
        }
        else{
            return redirect()->route('login');
        }

        $carts          = Carts::where('id_customer', $id_customer)->orderBy('created_at', 'DESC')->get();
        $carts_detail   = CartsDetail::where('id_customer', $id_customer)->get();

        return view('carts-detail/index',[
            'carts'         => $carts,
            'carts_detail'  => $carts_detail,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $id_customer    = Auth::id();
        $customer       = Customers::where('id', $id_customer)->first();
        $cart_detail    = CartsDetail::where('id_cart', $id)->get();
        return view('carts-detail/invoice',[
            'cart_detail'       => $cart_detail,
            'customer'          => $customer,
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function download($id_cart)
    {

        $data = ['title' => 'INVOICE HOLYWINGS'];

        $id_customer    = Auth::id();
        $customer       = Customers::where('id', $id_customer)->first();
        $cart_detail    = CartsDetail::where('id_cart', $id_cart)->get();

        $pdf = PDF::loadView('carts-detail/invoice_pdf',[
            'data'              => $data,
            'cart_detail'       => $cart_detail,
            'customer'          => $customer,
        ]);
        return $pdf->download('laporan-pdf.pdf');

        return view('carts-detail/index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
