<?php

namespace App\Http\Controllers;

use App\Models\Customers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class CustomersApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        $password   = Hash::make($request->password);

        $this->validate($request,[
            'name',
            'no_handphone',
            'password',
            'address',
        ]);

        $customers = Customers::create([
            'name'              => $request->name,
            'no_handphone'      => $request->no_handphone,
            'password'          => Hash::make($request->password),
            'address'           => $request->address,
        ]);

        return response()->json($customers, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $model  = Customers::where('id', $id)->first();

        return $model;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'id',
            'name',
            'no_handphone',
            'address',
        ]);

        $data = Customers::where('id', $request->id)->first();

        $data->name             = $request->name;
        $data->no_handphone     = $request->no_handphone;
        $data->address          = $request->address;

        $data->save();

        return response()->json($data, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data   = Customers::where('id',$id)->delete();

        return response()->json($data, 204);
    }
}
