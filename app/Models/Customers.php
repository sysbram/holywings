<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User  as Authenticatable;
use Illuminate\Database\Eloquent\Model;

class Customers extends Authenticatable
{
    protected $table = 'customers';
    
    protected $fillable = [
        'name', 'no_handphone', 'password', 'address',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'passcode', 'remember_token',
    ];
    
    public function getAuthPassword()
    {
      return $this->passcode;
    }
}
