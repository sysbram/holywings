<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Items extends Model
{
    protected $table = 'items';

    protected $fillable = [
        'id_items',
        'id_categorie',
        'name',
        'price',
        'image',
    ];

    public function getDataCategorie(){
        return $this->belongsTo(Categories::class,'id_catagorie', 'id_catagorie');
    }
}
