<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Carts extends Model
{
    protected $table = 'carts';

    protected $fillable = [
        'id_customer',
        'id_item',
        'status',
    ];

    public function getDataItems(){
        return $this->belongsTo(Items::class,'id_items', 'id_items');
    }
}
