<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CartsDetail extends Model
{
    protected $table = 'carts_detail';

    protected $fillable = [
        'id_cart',
        'id_customer',
        'id_item',
        'quantity',
        'total',
    ];

    public function getDataItems(){
        return $this->belongsTo(Items::class,'id_item', 'id_items');
    }

    public function getDataCategorie(){
        return $this->belongsTo(Categories::class,'id_catagorie', 'id_catagorie');
    }
}
