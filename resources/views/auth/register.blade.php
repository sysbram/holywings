@extends('login/register')
@section('title','Register Account')

@section('css')
@endsection


@section('content')
<form method="POST" action="{{ route('register') }}">
    @csrf


    <div class="form-group">
        <label class="text-label" for="email_2">Name</label>
        <div class="input-group input-group-merge">
            <input id="name" name="name" type="text" class="form-control form-control-prepended @error('name') is-invalid @enderror" placeholder="Enter your name" required autocomplete="name" autofocus>
            <div class="input-group-prepend">
                <div class="input-group-text">
                    <span class="fa fa-user"></span>
                </div>
            </div>
            @error('name')
            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
            @enderror
        </div>
    </div>
    <div class="form-group">
        <label class="text-label" for="no_handphone">No Handphone</label>
        <div class="input-group input-group-merge">
            <input id="no_handphone" name="no_handphone" type="number" class="form-control form-control-prepended @error('no_handphone') is-invalid @enderror" placeholder="Enter your number phone" required autocomplete="no_handphone" autofocus>
            <div class="input-group-prepend">
                <div class="input-group-text">
                    <span class="fa fa-phone"></span>
                </div>
            </div>
            @error('name')
            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
            @enderror
        </div>
    </div>
    <div class="form-group">
        <label class="text-label" for="password_2">Password</label>
        <div class="input-group input-group-merge">
            <input id="password" name="password" type="password" class="form-control form-control-prepended @error('password') is-invalid @enderror" placeholder="Enter your password" required autocomplete="new-password">
            <div class="input-group-prepend">
                <div class="input-group-text">
                    <span class="fa fa-key"></span>
                </div>
            </div>
            @error('password')
            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
            @enderror
        </div>
    </div>
    <div class="form-group">
        <label class="text-label" for="password_2">Password Confirm</label>
        <div class="input-group input-group-merge">
            <input id="password-confirm" name="password_confirmation" type="password" required autocomplete="new-password" class="form-control form-control-prepended" placeholder="Confirm your password">
            <div class="input-group-prepend">
                <div class="input-group-text">
                    <span class="fa fa-key"></span>
                </div>
            </div>

        </div>
    </div>
    <div class="form-group">
        <label class="text-label" for="address">Address</label>
        <div class="input-group input-group-merge">
            <input id="address" name="address" type="text" class="form-control form-control-prepended @error('address') is-invalid @enderror"  required autocomplete="address" placeholder="Enter your address">
            <div class="input-group-prepend">
                <div class="input-group-text">
                    <span class="fa fa-map-marker"></span>
                </div>
            </div>
            @error('address')
            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
            @enderror
        </div>
    </div>
    <div class="form-group mb-5">
        <div class="custom-control custom-checkbox">
            <input type="checkbox" class="custom-control-input" checked="" id="remember">
            <label class="custom-control-label text-warning" for="remember">Remember me</label>
        </div>
    </div>
    <div class="form-group text-center">
        <button class="btn mb-5" style="background-color: #FFBA00; color: #181a1b;" type="submit">{{ __('Register') }}</button>&nbsp;&nbsp;&nbsp;
        <!--            <a href="">Forgot password?</a> <br>-->
        <!--            Don't have an account? <a class="text-body text-underline" href="signup.html">Sign up!</a>-->
    </div>
</form>
    <div class="row">
        <p class="text-warning">Have an account? <a href="{{ route('login') }}"><span class="text-primary">Login</span></a></p>
    </div>
@endsection

@section('js')
@endsection
