@extends('layouts/master')
@section('title','Categories')

@section('content')

<div class="container-fluid page__heading-container">
    <div class="page__heading d-flex align-items-center">
        <div class="flex">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb mb-0">
                    <li class="breadcrumb-item"><a href="#"><i class="material-icons icon-20pt">home</i></a></li>
                    <li class="breadcrumb-item active" aria-current="page">@yield('title')</li>
                </ol>
            </nav>
            <h1 class="m-0">@yield('title')</h1>
        </div>
        <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#createModal">Create <i class="material-icons">add</i></button>
    </div>
</div>

<div class="container-fluid page__container" style="height: 800px;">
    <div class="card">
        <div class="card-header card-header-large">
            <div class="row">
                <div class="col-md-12">
                    <h4 class="card-header__title">@yield('title')</h4>
                </div>
            </div>
        </div>
        <div class="card-body">
            <div class="row align-items-end">
                <div class="card-body">
                    <div class="row">
                        <div class="table-responsive m-t-40">
                            <table id="categories_table" class="table table-striped">
                                <thead>
                                    <tr class="text-center">
                                        <th style="width: 5%;">No. </th>
                                        <th>ID Categorie</th>
                                        <th>Name</th>
                                        <th style="width: 5%;">Action</th>
                                    </tr>
                                </thead>
                                <tbody class="text-center">

                                </tbody>                            
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--  Create Modal -->
<div id="createModal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="false">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-body">
                <div class="px-3">
                    <div class="d-flex justify-content-center mt-2 mb-4 navbar-light">
                        <a href="index.html" class="navbar-brand" style="min-width: 0">
                            <span>Create New Categorie</span>
                        </a>
                    </div>

                    <form method="POST" role="form" action="{{ route('categories.store') }}" enctype="multipart/form-data">
                    @csrf
                        <div class="form-group">
                            <label class="col-form-label">ID Categorie: <span class="wajib"></span></label>
                            <input name="id_categorie" id="id_categorie" class="form-control" type="text" required>
                        </div>

                        <div class="form-group">
                            <label class="col-form-label">Name: <span class="wajib"></span></label>
                            <input name="name" id="name" class="form-control" type="text" required>
                        </div>

                        <div class="form-group text-center">
                            <button type="button" class="btn btn-light my-2" data-dismiss="modal">Cancel</button>
                            <button class="btn btn-warning" type="submit">Save</button>
                        </div>
                    </form>
                </div>
            </div> <!-- // END .modal-body -->
        </div> <!-- // END .modal-content -->
    </div> <!-- // END .modal-dialog -->
</div> <!-- // END .modal -->

<div id="deleteModal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" aria-labelledby="Modal Delete" data-backdrop="false">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content bg-danger">
            <div class="modal-body text-center p-4">
                <i class="material-icons icon-40pt text-white mb-2">report_problem</i>
                <h4 class="text-white">Delete this Data ?</h4>
                <p class="text-white mt-3">Are you sure to delete this data ?</p>
                    <form method="POST" id="delete-form" action="javascript:void(0)">
                        @csrf
                        @method('DELETE')
                            <input name='id' id="deleteid" hidden>
                            <button type="button" class="btn btn-light my-2" data-dismiss="modal">Cancel</button>
                            <button type="submit" class="btn btn-warning " id="btn-delete">Delete</button>
                    </form>
            </div> <!-- // END .modal-body -->
        </div> <!-- // END .modal-content -->
    </div> <!-- // END .modal-dialog -->
</div> <!-- // END .modal -->

@endsection

@section('css')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css">
@endsection


@section('js')
<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>

<script>

$('#categories_table').DataTable({
    responsive: true,
    processing: false,
    serverSide: true,
    ajax: "{{ route("categories.getDataCategories") }}",
    columns: [
        {
            data: null, sortable: false, render: function (data, type, row, meta) {
                var i = meta.row + meta.settings._iDisplayStart + 1;
                return i
            }
        },
        { data: 'id_categorie', name: 'id_categorie'},
        { data: 'name', name: 'name'},
        { data: 'action', name: 'action'},
    ]
});

/* Delete Data */
$('#deleteModal').on('show.bs.modal', function (e) {
    let Id = $(e.relatedTarget).data('id');
    $('#deleteid').val(Id.toString());
});
$('#delete-form').submit(function (e) {
    e.preventDefault();
    var formData = new FormData(this);
    let Id = formData.get('id');
    $.ajax({
        url: 'destroy/' + Id,
        data: {_token: '{{csrf_token()}}'},
        type: 'DELETE',
        dataType: 'HTML',
        success: function (resp) {
            $("#deleteModal").modal("hide");
            location.reload();
        },
        error: function (data) {
            console.log(data);
        }
    });
});
</script>
@endsection