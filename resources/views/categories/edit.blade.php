@extends('layouts/master')
@section('title','Edit Categories')

@section('content')

<div class="container-fluid  page__heading-container">
    <div class="page__heading">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb mb-0">
                <li class="breadcrumb-item"><a href="#"><i class="material-icons icon-20pt">home</i></a></li>
                <li class="breadcrumb-item active" aria-current="page">@yield('title')</li>
            </ol>
        </nav>
        <h1 class="m-0">@yield('title')</h1>
    </div>
</div>

<div class="container-fluid page__container">
    <div class="row">
        <div class="col-lg">
            <div class="z-0">
                <ul class="nav nav-tabs nav-tabs-custom" role="tablist" id="mytabs">
                    <li class="nav-item">
                        <a href="#tab-1" class="nav-link active" data-toggle="tab" role="tab">
                            <span class="nav-link__count">Forms @yield('title')</span>
                        </a>
                    </li>
                </ul>
                <form method="GET" action="{{ route('categories.update', $model->id) }}"  enctype="multipart/form-data">
                @csrf
                    <div class="card">
                        <div class="card-body tab-content">
                            <div class="tab-pane active show fade" id="tab-1">

                                <div class="card card-form">
                                    <div class="row no-gutters">
                                        <div class="col-lg-4 card-body">
                                            <p><strong class="headings-color">Forms @yield('title') </strong></p>
                                            <p class="text-muted">Edit this Categories <span class="font-weight-bold"></span></p>
                                        </div>
                                        <div class="col-lg-8 card-form__body card-body">
                                            <div class="row">
                                                <div class="col-12 col-md-6 mb-3">
                                                    <div class="form-group">
                                                        <label for="username">ID Categoreis: <span class="wajib"></span></label>
                                                        <input class="form-control" id="id_categorie" name="id_categorie" value="{{ $model->id_categorie }}" type="text" required />
                                                    </div>

                                                </div>
                                                <div class="col-12 col-md-6 mb-3">
                                                    <div class="form-group">
                                                        <label for="username">Name: <span class="wajib"></span></label>
                                                        <input class="form-control" id="name" name="name" value="{{ $model->name }}" type="text"  required/>
                                                    </div>

                                                    
                                                    <input id="id" name="id" value="{{ $model->id }}" type="hidden" class="form-control">
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group text-center">
                                                        <button id="submit" class="btn btn-warning" type="submit">Save</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

</div>

@endsection

@section('js')

<script type="text/javascript">
$('.uang').mask("#.##0", {reverse: true});

</script>

@endsection
