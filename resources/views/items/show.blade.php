@extends('layouts/mastercustomers')
@section('title','Detail Item')

@section('content')

<div class="container-fluid  page__heading-container">
    <div class="page__heading">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb mb-0">
                <li class="breadcrumb-item"><a href="#"><i class="material-icons icon-20pt">home</i></a></li>
                <li class="breadcrumb-item active" aria-current="page">items / @yield('title')</li>
            </ol>
        </nav>
        <h1 class="m-0">@yield('title')</h1>
    </div>
</div>

<div class="container-fluid page__container">
    <div class="row">
        <div class="col-lg">
            <div class="z-0">
                <ul class="nav nav-tabs nav-tabs-custom" role="tablist" id="mytabs">
                    <li class="nav-item">
                        <a href="#tab-1" class="nav-link active" data-toggle="tab" role="tab">
                            <span class="nav-link__count">Forms @yield('title')</span>
                        </a>
                    </li>
                </ul>
                <div class="card">
                    <div class="card-body tab-content">
                        <div class="tab-pane active show fade" id="tab-1">

                        <form method="GET" action="{{ route('cart.store' ) }}"  enctype="multipart/form-data">
                                    @csrf
                            <div class="card card-form">
                                <div class="row no-gutters">
                                    <div class="col-lg-4 card-body">
                                        <p><strong class="headings-color">Forms @yield('title') </strong></p>
                                        <a href="{{ route('home') }}"><button class="btn btn-primary">Back</button></a>
                                    </div>
                                    <div class="col-lg-8 card-form__body card-body">
                                        <table class="table">
                                            <tbody>
                                                <tr>
                                                    <td style="width: 40%"><h6><i class="fa fa-wine-bottle"></i> Nama</h6></td>
                                                    <td style="width: 5%">:</td>
                                                    <td style=" padding-left: 2px;"><h6> {{ $model->name  ?? "-"}} </h6></td>
                                                </tr>

                                                <tr>
                                                    <td style="width: 40%"><h6><i class="fa fa-money-bill-alt"></i> Price</h6></td>
                                                    <td style="width: 5%">:</td>
                                                    <td style=" padding-left: 2px;"><h6> {{ "Rp. ".number_format($model->price, 0, '', '.' )  ?? "-"}} </h6></td>
                                                </tr>

                                                <tr>
                                                    <td style="width: 40%"><h6><i class="fa fa-images"></i> Image</h6></td>
                                                    <td style="width: 5%">:</td>
                                                    <td style=" padding-left: 2px;">
                                                        <img src="{{ asset('images/products') }}/{{ $model->image }}" style="width: 100%" alt="" class="card-img">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 40%"><h6> Quantity</h6></td>
                                                    <td style="width: 5%">:</td>
                                                    <td style=" padding-left: 2px;">
                                                        <input class="form-control" id="quantity" value="1" min="1" name="quantity" type="number" />
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td colspan="3">
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                            <div class="row">
                                                <div class="col-md-4"></div>
                                                <div class="col-md-4">
                                                    <input name="id_items" value="{{ $model->id_items }}" type="hidden">
                                                    <button class="btn btn-warning">Add to Cart</button>
                                                </div>
                                                <div class="col-md-4"></div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

@endsection

@section('js')

<script type="text/javascript">
$('.uang').mask("#.##0", {reverse: true});

</script>

@endsection
