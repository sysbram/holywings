@extends('layouts/master')
@section('title','Edit Item')

@section('content')

<div class="container-fluid  page__heading-container">
    <div class="page__heading">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb mb-0">
                <li class="breadcrumb-item"><a href="#"><i class="material-icons icon-20pt">home</i></a></li>
                <li class="breadcrumb-item active" aria-current="page">@yield('title')</li>
            </ol>
        </nav>
        <h1 class="m-0">@yield('title')</h1>
    </div>
</div>

<div class="container-fluid page__container">
    <div class="row">
        <div class="col-lg">
            <div class="z-0">
                <ul class="nav nav-tabs nav-tabs-custom" role="tablist" id="mytabs">
                    <li class="nav-item">
                        <a href="#tab-1" class="nav-link active" data-toggle="tab" role="tab">
                            <span class="nav-link__count">Forms @yield('title')</span>
                        </a>
                    </li>
                </ul>
                <form method="POST" action="{{ route('items.update' ) }}"  enctype="multipart/form-data">
                @csrf
                    <div class="card">
                        <div class="card-body tab-content">
                            <div class="tab-pane active show fade" id="tab-1">

                                <div class="card card-form">
                                    <div class="row no-gutters">
                                        <div class="col-lg-4 card-body">
                                            <p><strong class="headings-color">Forms @yield('title') </strong></p>
                                            <p class="text-muted">Edit this item <span class="font-weight-bold"></span></p>
                                        </div>
                                        <div class="col-lg-8 card-form__body card-body">
                                            <div class="row">
                                                <div class="col-12 col-md-6 mb-3">
                                                    <div class="form-group">
                                                        <label for="username">ID Items: <span class="wajib"></span></label>
                                                        <input class="form-control" id="id_items" name="id_items" value="{{ $model->id_items }}" type="text" />
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="username">Select Categories: <span class="wajib"></span></label>
                                                        <select name="id_categorie" id="id_categorie" class="form-control" required>
                                                            <option value="{{ $model->id_categorie }}">{{ $model->id_categorie }}</option>
                                                            @foreach ($data_categories as $data)
                                                            <option value="{{ $data->id_categorie }}">{{ $data->name }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>

                                                    <img src="{{ asset('images/products') }}/{{ $model->image }}" style="height: 340px;" alt="" class="card-img">
                                                    <div class="form-group">
                                                        <label class="col-form-label">Image:</label>
                                                        <input name="image" id="image" class="form-control" type="file">
                                                    </div>

                                                </div>
                                                <div class="col-12 col-md-6 mb-3">
                                                    <div class="form-group">
                                                        <label for="username">Name: <span class="wajib"></span></label>
                                                        <input class="form-control" id="name" name="name" value="{{ $model->name }}" type="text" />
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="price" class="col-form-label" >Price: <span class="wajib"></span></label>
                                                        <div class="input-group mb-3">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text">Rp. </span>
                                                            </div>
                                                            <input id="price" name="price" value="{{ $model->price }}" min="1" type="text" class="form-control uang">
                                                        </div>
                                                    </div>

                                                    
                                                    <input id="id" name="id" value="{{ $model->id }}" type="hidden" class="form-control">
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group text-center">
                                                        <button id="submit" class="btn btn-warning" type="submit">Save</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

</div>

@endsection

@section('js')

<script type="text/javascript">
$('.uang').mask("#.##0", {reverse: true});

</script>

@endsection
