@extends('layouts/master')
@section('title','Items')

@section('content')

<div class="container-fluid page__heading-container">
    <div class="page__heading d-flex align-items-center">
        <div class="flex">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb mb-0">
                    <li class="breadcrumb-item"><a href="#"><i class="material-icons icon-20pt">home</i></a></li>
                    <li class="breadcrumb-item active" aria-current="page">@yield('title')</li>
                </ol>
            </nav>
            <h1 class="m-0">@yield('title')</h1>
        </div>
        <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#createModal">Create <i class="material-icons">add</i></button>
    </div>
</div>

<div class="container-fluid page__container" style="height: 800px;">
    <div class="card">
        <div class="card-header card-header-large">
            <div class="row">
                <div class="col-md-12">
                    <h4 class="card-header__title">@yield('title')</h4>
                </div>
            </div>
        </div>
        <div class="card-body">
            <div class="row align-items-end">
                <div class="card-body">
                    <div class="row">
                        <div class="table-responsive m-t-40">
                            <table id="items_table" class="table table-striped">
                                <thead>
                                    <tr class="text-center">
                                        <th style="width: 5%;">No. </th>
                                        <th>Name</th>
                                        <th>Price (Rp)</th>
                                        <th>Image</th>
                                        <th style="width: 5%;">Action</th>
                                    </tr>
                                </thead>
                                <tbody class="text-center">

                                </tbody>                            
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--  Create Modal -->
<div id="createModal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="false">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-body">
                <div class="px-3">
                    <div class="d-flex justify-content-center mt-2 mb-4 navbar-light">
                        <a href="index.html" class="navbar-brand" style="min-width: 0">
                            <span>Create New Items</span>
                        </a>
                    </div>

                    <form method="POST" role="form" action="{{ route('items.store') }}" enctype="multipart/form-data">
                    @csrf
                        <div class="form-group">
                            <label for="username">Select Categories:</label>
                            <select name="id_categorie" id="id_categorie" class="form-control" required>
                                <option value="">== Select Data Categories ==</option>
                                @foreach ($data_categories as $data)
                                <option value="{{ $data->id_categorie }}">{{ $data->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="col-form-label">Name: <span class="wajib"></span></label>
                            <input name="name" id="name" class="form-control" type="text" required>
                        </div>

                        <div class="form-group">
                            <label for="price" class="col-form-label" >Price:</label>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Rp. </span>
                                </div>
                                <input id="price" name="price" min="1" type="text" class="form-control uang">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-form-label">Image: <span class="wajib"></span></label>
                            <input name="image" id="image" class="form-control" type="file" required>
                        </div>

                        <div class="form-group text-center">
                            <button type="button" class="btn btn-light my-2" data-dismiss="modal">Cancel</button>
                            <button class="btn btn-warning" type="submit">Save</button>
                        </div>
                    </form>
                </div>
            </div> <!-- // END .modal-body -->
        </div> <!-- // END .modal-content -->
    </div> <!-- // END .modal-dialog -->
</div> <!-- // END .modal -->

<div id="deleteModal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" aria-labelledby="Modal Delete" data-backdrop="false">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content bg-danger">
            <div class="modal-body text-center p-4">
                <i class="material-icons icon-40pt text-white mb-2">report_problem</i>
                <h4 class="text-white">Delete this Data ?</h4>
                <p class="text-white mt-3">Are you sure to delete this data ?</p>
                    <form method="POST" id="delete-form" action="javascript:void(0)">
                        @csrf
                        @method('DELETE')
                            <input name='id' id="deleteid" hidden>
                            <button type="button" class="btn btn-light my-2" data-dismiss="modal">Cancel</button>
                            <button type="submit" class="btn btn-warning " id="btn-delete">Delete</button>
                    </form>
            </div> <!-- // END .modal-body -->
        </div> <!-- // END .modal-content -->
    </div> <!-- // END .modal-dialog -->
</div> <!-- // END .modal -->

@endsection

@section('css')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css">
@endsection


@section('js')
<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>

<script>

$('#items_table').DataTable({
    responsive: true,
    processing: false,
    serverSide: true,
    ajax: "{{ route("items.getDataItems") }}",
    columns: [
        {
            data: null, sortable: false, render: function (data, type, row, meta) {
                var i = meta.row + meta.settings._iDisplayStart + 1;
                return i
            }
        },
        { data: 'name', name: 'name'},
        { data: 'price', name: 'price'},
        { data: 'image', name: 'image'},
        { data: 'action', name: 'action'},
    ]
});

/* Delete Data */
$('#deleteModal').on('show.bs.modal', function (e) {
    let Id = $(e.relatedTarget).data('id');
    $('#deleteid').val(Id.toString());
});
$('#delete-form').submit(function (e) {
    e.preventDefault();
    var formData = new FormData(this);
    let Id = formData.get('id');
    $.ajax({
        url: 'destroy/' + Id,
        data: {_token: '{{csrf_token()}}'},
        type: 'DELETE',
        dataType: 'HTML',
        success: function (resp) {
            $("#deleteModal").modal("hide");
            location.reload();
        },
        error: function (data) {
            console.log(data);
        }
    });
});

$('.uang').mask("#.##0", {reverse: true});
</script>
@endsection