<div class="mdk-drawer  js-mdk-drawer" id="default-drawer" data-align="start">
    <div
        class="mdk-drawer__content">
        <div class="sidebar sidebar-light sidebar-left simplebar" data-simplebar>
            @if(!Auth::user())
            <div class="d-flex align-items-center sidebar-p-a border-bottom sidebar-account">
                <a href="{{ route('login') }}" class="flex d-flex align-items-center text-underline-0 text-body">
                                    <span class="avatar mr-3">
                                        <img src="{{ asset('assets/theme/dist') }}/assets/images/logo.png" alt="avatar" class="avatar-img rounded-circle">
                                    </span>
                    <span class="flex d-flex flex-column"><strong>LOGIN</strong></span>
                </a>
            </div>
            @else
            <div class="d-flex align-items-center sidebar-p-a border-bottom sidebar-account">
                <a href="profile.html" class="flex d-flex align-items-center text-underline-0 text-body">
                                    <span class="avatar mr-3">
                                        <img src="{{ asset('assets/theme/dist') }}/assets/images/avatar/demi.png" alt="avatar" class="avatar-img rounded-circle">
                                    </span>
                    <span class="flex d-flex flex-column">
                            <strong>{{ Auth::user()->name }}</strong>
                            <!--<small class="text-muted text-uppercase">Account Manager</small>-->
                    </span>
                </a>

                <div class="dropdown ml-auto">
                    <a href="#" data-toggle="dropdown" data-caret="false" class="text-muted"><i class="material-icons">more_vert</i></a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <div class="dropdown-item-text dropdown-item-text--lh">
                            <div><strong>{{ Auth::user()->name }}</strong></div>
                        </div>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="{{ route('home') }}">Dashboard</a>
                        <!--                        <a class="dropdown-item" href="profile.html">My profile</a>-->
                        <!--                        <a class="dropdown-item" href="edit-account.html">Edit account</a>-->
                        <!--                        <div class="dropdown-divider"></div>-->
                        <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                            @csrf
                        </form>
                    </div>
                </div>
            </div>
            @endif
            @if(Auth::id() == 1)
            <div class="sidebar-heading sidebar-m-t">Admin Portal</div>
                <ul class="sidebar-menu">
                    <li class="sidebar-menu-item {{ request()->is('home/*') ? 'active open' : '' }}">
                        <a class="sidebar-menu-button" href="{{ route('home') }}">
                            <i class="sidebar-menu-icon sidebar-menu-icon--left material-icons">dashboard</i>
                            <span class="sidebar-menu-text">Dashboard</span>
                        </a>
                    </li>

                    <li class="sidebar-menu-item {{ request()->is('customers/*') ? 'active open' : '' }}">
                        <a class="sidebar-menu-button" href="{{ route('customers.index') }}">
                            <i class="sidebar-menu-icon sidebar-menu-icon--left material-icons">account_circle</i>
                            <span class="sidebar-menu-text">Customers</span>
                        </a>
                    </li>
                    
                    <?php
                        if(request()->is('items/*') || request()->is('categories/*'))
                            $class='active open';
                        else
                            $class='-';
                    ?>
                    <li class="{{$class}}">
                        <a class="sidebar-menu-button" data-toggle="collapse" href="#items">
                            <i class="sidebar-menu-icon sidebar-menu-icon--left material-icons">loyalty</i>
                            <span class="sidebar-menu-text">Items</span>
                            <span class="ml-auto sidebar-menu-toggle-icon"></span>
                        </a>
                        <ul class="sidebar-submenu collapse" id="categories">
                            <li class="sidebar-menu-item">
                                <a class="sidebar-menu-button" href="{{route('categories.index')}}">
                                    <span class="sidebar-menu-text">Category</span>
                                </a>
                            </li>
                        </ul>
                        <ul class="sidebar-submenu collapse" id="items">
                            <li class="sidebar-menu-item">
                                <a class="sidebar-menu-button" href="{{route('items.index')}}">
                                    <span class="sidebar-menu-text">Items</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>

                
            </div>
            
            
            @endif
        </div>
    </div>
</div>
