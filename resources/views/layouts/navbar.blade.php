<div id="header" class="mdk-header js-mdk-header m-0" data-fixed>
    <div class="mdk-header__content">
        <div class="navbar navbar-expand-sm navbar-main navbar-dark pr-0" id="navbar" data-primary style="background-color: #181a1b;;">
            <div class="container-fluid p-0">
                <!-- Navbar toggler -->
                <button class="navbar-toggler navbar-toggler-right d-block d-md-none" type="button" data-toggle="sidebar">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <!-- Navbar Brand -->
                <a href="{{ route('home') }}" class="navbar-brand ">
                    <!-- <img class="navbar-brand-icon" src="{{ asset('assets/theme/dist') }}/assets/images/stack-logo-white.svg" width="22" alt="Stack"> -->
                    <img class="navbar-brand-icon" src="{{ asset('assets/theme/dist') }}/assets/images/logo.png" width="100" alt="Stack">    
                </a>




                @guest
                <ul class="nav navbar-nav ml-auto d-none d-md-flex">
                    <li class="nav-item">
                        <a href="{{ route('login') }}" class="nav-link">
                            <i class="material-icons"></i> {{ __('Login') }}
                        </a>
                    </li>
                    @if (Route::has('register'))
                    <li class="nav-item">
                        <a href="{{ route('register') }}" class="nav-link">
                            <i class="material-icons"></i> {{ __('Register') }}
                        </a>
                    </li>
                    @endif
                </ul>

                @else
                <ul class="nav navbar-nav d-none d-sm-flex border-left navbar-height align-items-center">
                    <li class="nav-item dropdown">
                        <a href="#account_menu" class="nav-link dropdown-toggle" data-toggle="dropdown" data-caret="false">
                            <img src="{{ asset('assets/theme/dist') }}/assets/images/avatar/demi.png" class="rounded-circle" width="32" alt="Frontted">
                            <span class="ml-1 d-flex-inline"><span class="text-light">{{ Auth::user()->name }}</span></span>
                        </a>

                        <div id="account_menu" class="dropdown-menu dropdown-menu-right">
                            <div class="dropdown-item-text dropdown-item-text--lh">
                                <div><strong>{{ Auth::user()->name }}</strong></div>
                            </div>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="{{ route('home') }}">Dashboard</a>
                            <a class="dropdown-item" href="{{ route('cartsDetail.index') }}">Carts</a>
                            @if(Auth::id() == 1)
                                <a class="dropdown-item" href="{{ route('items.index') }}">Add Data</a>
                            @endif
                            <!--                            <a class="dropdown-item" href="profile.html">My profile</a>-->
                            <!--                            <a class="dropdown-item" href="edit-account.html">Edit account</a>-->
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                @csrf
                            </form>
                        </div>
                    </li>
                </ul>
                @endguest

            </div>
        </div>
    </div>
</div>
