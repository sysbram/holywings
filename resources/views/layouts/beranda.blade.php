@extends('layouts/master')
@section('title','Dashboard')

@section('css')
@endsection


@section('content')
<div class="mdk-drawer-layout__content page">
    <div class="container-fluid page__heading-container">
        <div class="page__heading d-flex align-items-center">
            <!-- <a href="" class="btn btn-light ml-3"><i class="material-icons icon-16pt text-muted mr-1">settings</i> Settings</a> -->
        </div>
    </div>


    <div class="container-fluid page__container">
        <div class="row">

            @foreach ($items as $item)
                <div class="col-sm-6 col-md-4">
                    <a href="{{ route('items.show', $item->id) }}"> 
                        <div class="card stories-card-popular" style="height: 400px;">
                            <img src="{{ asset('images/products') }}/{{ $item->image }}" style="height: 340px;" alt="" class="card-img">
                            <div class="stories-card-popular__content">
                                <div class="card-body d-flex align-items-center">
                                </div>
                            </div>
                        </div>
                        <div class="stories-card-popular__title card-body" style="padding-bottom:30px;">
                            <small class="text-primary">{{ "Rp. ".number_format($item->price, 0, '', '.') }}</small>
                            <h4 class="card-title text-primary m-0">{{ $item->name }}</h4>
                        </div>
                    </a>
                </div>
            
            @endforeach
        </div>
            
        </div>
    </div>


</div>
<!-- // END drawer-layout__content -->
@endsection

@section('js')
@endsection
