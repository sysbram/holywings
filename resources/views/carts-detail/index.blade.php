@extends('layouts/mastercustomers')
@section('title','Carts')

@section('content')

<div class="container-fluid page__heading-container">
    <div class="page__heading d-flex align-items-center">
        <div class="flex">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb mb-0">
                    <li class="breadcrumb-item"><a href="#"><i class="material-icons icon-20pt">home</i></a></li>
                    <li class="breadcrumb-item active" aria-current="page">@yield('title')</li>
                </ol>
            </nav>
            <h1 class="m-0">@yield('title')</h1>
        </div>
        <a href="{{ route('home') }}" ><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#createModal">Back <i class="material-icons"></i></button></a>
    </div>
</div>

<div class="container-fluid page__container">
    <div class="row">
        <div class="col-lg">
            <div class="z-0">
                <ul class="nav nav-tabs nav-tabs-custom" role="tablist" id="mytabs">
                    <li class="nav-item">
                        <a href="#tab-1" class="nav-link active" data-toggle="tab" role="tab">
                            <span class="nav-link__count">@yield('title')</span>
                        </a>
                    </li>
                </ul>
                <div class="card">
                    <div class="card-body tab-content">
                        <div class="tab-pane active show fade" id="tab-1">
                            @foreach ($carts as $cart)
                            <div class="card card-form">
                                <div class="row no-gutters">
                                    <div class="col-lg-4 card-body">
                                        <p><strong class="headings-color">
                                            @if ($cart->status == 1)
                                                <span class="text-warning">Pending</span>
                                                <form method="GET" action="{{ route('cart.update', $cart->id ) }}"  enctype="multipart/form-data">
                                                @csrf
                                                    <button class="btn btn-warning">Pay</button>
                                                </form>
                                            @elseif ($cart->status == 2)
                                                <span class="text-success">Success</span>
                                                <form method="GET" action="{{ route('cartsDetail.show', $cart->id ) }}"  enctype="multipart/form-data">
                                                @csrf
                                                    <button class="btn btn-success">Print Invoice</button>
                                                </form>
                                            @endif
                                            
                                        </strong></p>
                                    </div>
                                    <div class="col-lg-8 card-form__body card-body">
                                        <table class="table">
                                            <tbody>
                                                <?php $total = 0; ?>
                                                @foreach ($carts_detail as $detail)
                                                    @if ($detail->id_cart == $cart->id)
                                                    <?php $total += $detail->total ?>
                                                    <tr>
                                                        <td style="width: 40%"><h6><i class="fa fa-wine-bottle"></i> Nama</h6></td>
                                                        <td style="width: 5%">:</td>
                                                        <td style=" padding-left: 2px;"><h6> {{ $detail->getDataItems->name  ?? "-"}} </h6></td>
                                                    </tr>

                                                    <tr>
                                                        <td style="width: 40%"><h6><i class="fa fa-money-bill-alt"></i> Price</h6></td>
                                                        <td style="width: 5%">:</td>
                                                        <td style=" padding-left: 2px;"><h6> {{ "Rp. ".number_format($detail->getDataItems->price, 0, '', '.' )  ?? "-"}} </h6></td>
                                                    </tr>

                                                    <tr>
                                                        <td style="width: 40%"><h6><i class="fa fa-calculator"></i> Quantity</h6></td>
                                                        <td style="width: 5%">:</td>
                                                        <td style=" padding-left: 2px;"><h6> {{ $detail->quantity  ?? "-"}} </h6></td>
                                                    </tr>

                                                    <tr>
                                                        <td style="width: 40%"><h6><i class="fa fa-money-bill-alt"></i> Total</h6></td>
                                                        <td style="width: 5%">:</td>
                                                        <td style=" padding-left: 2px;"><h6> {{ "Rp. ".number_format($detail->total, 0, '', '.' ) ?? "-"}} </h6></td>
                                                    </tr>

                                                    <tr>
                                                        <td style="width: 40%"><h6><i class="fa fa-images"></i> Image</h6></td>
                                                        <td style="width: 5%">:</td>
                                                        <td style=" padding-left: 2px;">
                                                            <img src="{{ asset('images/products') }}/{{ $detail->getDataItems->image }}" style="width: 80px;" alt="" class="card-img">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3"><hr></td>
                                                    </tr>
                                                    @endif
                                                @endforeach
                                            </tbody>
                                        </table>
                                        <table class="table">
                                            <tbody>
                                                <tr>
                                                    <td style="width: 40%"><h6><i class="fa fa-money-bill-alt"></i> TOTAL</h6></td>
                                                    <td style="width: 5%">:</td>
                                                    <td style=" padding-left: 2px;"><h6> {{ "Rp. ".number_format($total, 0, '', '.' )  ?? "-"}} </h6></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="3"><hr></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

@endsection

@section('js')

<script type="text/javascript">
$('.uang').mask("#.##0", {reverse: true});

</script>

@endsection
