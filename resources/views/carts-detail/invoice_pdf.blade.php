<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>@yield("title","HOLYWINGS")</title>
    <meta name="robots" content="noindex">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('favicon.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('favicon.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('favicon.png') }}">

    <!-- Simplebar -->
    <link type="text/css" href="{{ asset('assets/theme/dist') }}/assets/vendor/simplebar.min.css" rel="stylesheet">

    <!-- App CSS -->
    <link type="text/css" href="{{ asset('assets/theme/dist') }}/assets/css/app.css" rel="stylesheet">
    <link type="text/css" href="{{ asset('assets/theme/dist') }}/assets/css/app.rtl.css" rel="stylesheet">

    <!-- Material Design Icons -->

    <link href="https://ajax.aspnetcdn.com/ajax/jquery.ui/1.8.9/themes/blitzer/jquery-ui.css" rel="stylesheet" type="text/css" />
    
    <!-- Select 2 -->
    <link type="text/css" href="{{ asset('assets/theme/dist') }}/assets/css/vendor-select2.css" rel="stylesheet">
    <link type="text/css" href="{{ asset('assets/theme/dist') }}/assets/css/vendor-select2.rtl.css" rel="stylesheet">
    <link type="text/css" href="{{ asset('assets/theme/dist') }}/assets/vendor/select2/select2.min.css" rel="stylesheet">
</head>
<body class="layout-default">
<div class="mdk-header-layout__content">
    <div class="mdk-drawer-layout js-mdk-drawer-layout" data-push data-responsive-width="992px">
        <div class="mdk-drawer-layout__content page">
            <div class="container-fluid page__container">
                <div class="col-md-8 offset-md-2">
                    <div class="card">
                        <div class="card-body">
                            <div class="badge badge-success">SUCCESS</div>

                            <div class="px-3">
                                <div class="d-flex justify-content-center flex-column text-center my-5 navbar-light">
                                    <a href="index.html" class="navbar-brand d-flex flex-column m-0" style="min-width: 0">
                                        <img class="navbar-brand-icon mb-2" src="{{ asset('assets/theme/dist') }}/assets/images/logo.png" width="100" alt="Stack">  
                                        <span>Invoice</span>
                                    </a>
                                    <div class="text-muted">Invoice #MPS1234AD</div>
                                </div>
                                <div class="row mb-5">
                                    <div class="col-lg">
                                        <div class="text-label">FROM</div>
                                        <p class="mb-4">
                                            <strong class="text-body">Holywings</strong><br>
                                            JL. BOULEVARD RAYA BARAT NO 5<br>
                                            KLP GADING, JAKARTA UTARA<br>
                                        </p>
                                        <div class="text-label">Invoice NUMBER</div>
                                        #MPS1234AD
                                    </div>
                                    <div class="col-lg text-right">
                                        <div class="text-label">TO (CUSTOMER)</div>
                                        <p class="mb-4">
                                            <strong class="text-body">{{ $customer->name }}</strong><br>
                                            {{ $customer->address }}<br><br>
                                        </p>
                                        <div class="text-label">Date</div>
                                        {{ $cart_detail[0]['updated_at'] }}
                                    </div>
                                </div>

                                <div class="table-responsive">
                                    <table class="table border-bottom mb-5">
                                        <thead>
                                            <tr class="bg-light">
                                                <th>Item Name</th>
                                                <th class="text-right">Cost</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $sum = 0 ?>
                                            @foreach ($cart_detail as $cart)
                                                <tr>
                                                    <td>{{ $cart->getDataItems->name }}</td>
                                                    <td class="text-right">{{ number_format($cart->getDataItems->price, 0, '', '.' ) }}</td>
                                                    <?php $sum += $cart->getDataItems->price; ?>
                                                </tr>
                                            @endforeach
                                            <tr class="bg-light">
                                                <th>TOTAL</th>
                                                <th class="text-right">{{ number_format($sum, 0, '', '.' ) }}</th>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>

                                <p class="text-muted">Thank you for comming! HOLYWINGS!!!</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>