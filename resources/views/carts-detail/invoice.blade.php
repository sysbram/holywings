@extends('layouts/mastercustomers')
@section('title','Invoice')

@section('content')

<div class="container-fluid page__heading-container">
    <div class="page__heading d-flex align-items-center">
        <div class="flex">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb mb-0">
                    <li class="breadcrumb-item"><a href="#"><i class="material-icons icon-20pt">home</i></a></li>
                    <li class="breadcrumb-item active" aria-current="page">@yield('title')</li>
                </ol>
            </nav>
            <h1 class="m-0">@yield('title')</h1>
        </div>
        <a href="{{ route('cartsDetail.download', $cart_detail[0]['id_cart']) }}" ><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#createModal">Download</button></a>
    </div>
</div>

<div class="container-fluid page__container">
    <div class="col-md-8 offset-md-2">
        <div class="card">
            <div class="card-body">
            <div class="badge badge-success">SUCCESS</div>

                <div class="px-3">
                    <div class="d-flex justify-content-center flex-column text-center my-5 navbar-light">
                        <a href="index.html" class="navbar-brand d-flex flex-column m-0" style="min-width: 0">
                            <img class="navbar-brand-icon mb-2" src="{{ asset('assets/theme/dist') }}/assets/images/logo.png" width="100" alt="Stack">  
                            <span>Invoice</span>
                        </a>
                        <div class="text-muted">Invoice #MPS1234AD</div>
                    </div>
                    <div class="row mb-5">
                        <div class="col-lg">
                            <div class="text-label">FROM</div>
                            <p class="mb-4">
                                <strong class="text-body">Holywings</strong><br>
                                JL. BOULEVARD RAYA BARAT NO 5<br>
                                KLP GADING, JAKARTA UTARA<br>
                            </p>
                            <div class="text-label">Invoice NUMBER</div>
                            #MPS1234AD
                        </div>
                        <div class="col-lg text-right">
                            <div class="text-label">TO (CUSTOMER)</div>
                            <p class="mb-4">
                                <strong class="text-body">{{ $customer->name }}</strong><br>
                                {{ $customer->address }}<br><br>
                            </p>
                            <div class="text-label">Date</div>
                            {{ $cart_detail[0]['updated_at'] }}
                        </div>
                    </div>

                    <div class="table-responsive">
                        <table class="table border-bottom mb-5">
                            <thead>
                                <tr class="bg-light text-center">
                                    <th>Item Name</th>
                                    <th>Quantity</th>
                                    <th>Cost</th>
                                    <th class="text-right"> Total Cost</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $sum = 0 ?>
                                @foreach ($cart_detail as $cart)
                                    <tr>
                                        <td>{{ $cart->getDataItems->name }}</td>
                                        <td>{{ $cart->quantity }}</td>
                                        <td class="text-right">{{ number_format($cart->getDataItems->price, 0, '', '.' ) }}</td>
                                        <td class="text-right">{{ number_format($cart->total, 0, '', '.' ) }}</td>
                                        <?php $sum += $cart->total; ?>
                                    </tr>
                                @endforeach
                                <tr class="bg-light">
                                    <th colspan="3">TOTAL</th>
                                    <th class="text-right">{{ number_format($sum, 0, '', '.' ) }}</th>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                    <p class="text-muted">Thank you for comming! HOLYWINGS!!!</p>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('js')

<script type="text/javascript">
$('.uang').mask("#.##0", {reverse: true});

</script>

@endsection
